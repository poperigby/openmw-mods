# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Wyrmhaven"
    DESC = "Adds Wyrmhaven, a tiny island far to the west of Solstheim"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/42933"
    # License is still identical to the one provided in wyrmhaven-2.0
    LICENSE = "wyrmhaven-2.0"
    RESTRICT = "mirror"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        landmasses/tamriel-data
    """
    KEYWORDS = "openmw"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/42933?tab=files&file_id=1000027594
        -> Wyrmhaven-42933-3-0-1635561356.7z
    """
    TEXTURE_SIZES = "1024"
    INSTALL_DIRS = [
        InstallDir(
            ".",
            PLUGINS=[File("Wyrmhaven.esm")],
            GROUNDCOVER=[File("Wyrmhaven - Grass.esp")],
            DOC=["Wyrmhaven - Readme.txt"],
        )
    ]

    def src_pretend(self):
        self.warn("Wyrmhaven 3.0 is not save-game compatible with earlier versions")
