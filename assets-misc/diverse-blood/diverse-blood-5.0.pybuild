# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    DESC = "8 new blood textures for NPCs and creatures. 1k quality."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45368"
    KEYWORDS = "openmw"
    LICENSE = "attribution-derivation"
    NAME = "Diverse Blood"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45368"
    IUSE = "dngdr-edit qwerty-edit rebirth"
    RDEPEND = """
        base/morrowind[bloodmoon,tribunal]
        rebirth? ( base/morrowind-rebirth )
    """
    TEXTURE_SIZES = "1024"
    SRC_URI = """
        Diverse_Blood-45368-1-41-1561159970.7z
        dngdr-edit? ( Diverse_Blood_-_DNGDR_Edit-45368-1-0-1576966369.7z )
        qwerty-edit? ( Diverse_Blood_-_Qwerty_Edit-45368-1-0-1543984457.7z )
        rebirth? ( Diverse_Blood_-_Rebirth_Addon-45368-5-0-1578791228.7z )
    """
    FALLBACK = {
        "Blood": {
            "Texture_3": "Blood_Energy.dds",
            "Texture_4": "Blood_Blue.dds",
            "Texture_5": "Blood_Green.dds",
            "Texture_6": "Blood_Dark.dds",
            "Texture_7": "Blood_Orange.dds",
        }
    }
    INSTALL_DIRS = [
        InstallDir(
            "Data Files",
            PLUGINS=[File("Bob's Diverse Blood.ESP")],
            S="Diverse_Blood-45368-1-41-1561159970",
        ),
        InstallDir(
            "Data Files",
            PLUGINS=[File("Bob's Diverse Blood - DNGDR Edit.ESP")],
            S="Diverse_Blood_-_DNGDR_Edit-45368-1-0-1576966369",
            REQUIRED_USE="dngdr-edit",
        ),
        InstallDir(
            "Data Files",
            S="Diverse_Blood_-_Qwerty_Edit-45368-1-0-1543984457",
            REQUIRED_USE="qwerty-edit",
        ),
        InstallDir(
            "Data Files",
            PLUGINS=[File("Bob's Diverse Blood - Rebirth Addon.ESP")],
            S="Diverse_Blood_-_Rebirth_Addon-45368-5-0-1578791228",
            REQUIRED_USE="rebirth",
        ),
    ]
