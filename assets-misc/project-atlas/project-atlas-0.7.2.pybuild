# Copyright Copyright 2019-2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
import sys

from common.atlasgen import AtlasGen
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod

MAIN_FILE = "Project_Atlas-45399-0-7-1-1644694673"


class Package(AtlasGen, NexusMod, MW):
    NAME = "Project Atlas"
    DESC = "Texture atlases to improve performance"
    HOMEPAGE = """
        https://www.nexusmods.com/morrowind/mods/45399
        https://github.com/revenorror/Project-Atlas
    """
    LICENSE = "all-rights-reserved"
    RDEPEND = """
        graphic-herbalism? ( gameplay-misc/graphic-herbalism )
        gitd? ( arch-misc/glow-in-the-dahrk )
        !atlasgen? (
            met? ( assets-textures/morrowind-enhanced-textures )
            intelligent-textures? ( assets-textures/intelligent-textures )
        )
    """
    DEPEND = "virtual/imagemagick"
    KEYWORDS = "openmw"
    S = "Project_Atlas-45399-0-6-5-1583081881"
    NEXUS_SRC_URI = f"""
        https://nexusmods.com/morrowind/mods/45399?tab=files&file_id=1000029284
            -> {MAIN_FILE}.7z
        https://nexusmods.com/morrowind/mods/45399?tab=files&file_id=1000030096
            -> Hotfix_0.7.2-45399-0-7-2-1649834953.7z
        !atlasgen? (
            intelligent-textures? (
                https://nexusmods.com/morrowind/mods/45399?tab=files&file_id=1000029285
                -> Textures_-_Intelligent_Textures-45399-0-7-1-1644694801.7z
            )
            met? (
                https://nexusmods.com/morrowind/mods/45399?tab=files&file_id=1000029286
                -> Textures_-_MET-45399-0-7-1-1644695018.7z
            )
        )
    """
    IUSE = "gitd glowing-bitter-coast met intelligent-textures graphic-herbalism"
    DATA_OVERRIDES = """
        assets-misc/morrowind-optimization-patch
        gameplay-misc/graphic-herbalism
    """
    TEXTURE_SIZES = "512 1024"
    TIER = "z"
    PATCHES = "ashtree.patch furn_de.patch limeware.patch"
    S = MAIN_FILE

    INSTALL_DIRS = [
        InstallDir(
            "00 BATs/Textures",
            ATLASGEN=[
                File("_ac_trees_atlas_generator.bat"),
                File("_ashtree_atlas_generator.bat"),
                File("_barnacle_atlas_generator.bat"),
                File("_bc_mushrooms_atlas_generator.bat"),
                File("_bm_cavedoor_atlas_generator.bat"),
                File("_coins_atlas_generator.bat"),
                File("_colony_atlas_generator.bat"),
                File("_dae_stat_atlas_generator.bat"),
                File("_doors_atlas_generator.bat"),
                File("_dragonstatue_atlas_generator.bat"),
                File("_emperor_parasol_atlas_generator.bat"),
                File("_furn_basket_atlas_generator.bat"),
                File("_furn_de_atlas_generator.bat"),
                File("_hlaalu_atlas_generator.bat"),
                File("_imperial_atlas_generator.bat"),
                File("_kelp_atlas_generator.bat"),
                File("_limeware_atlas_generator.bat"),
                File("_nord_atlas_generator.bat"),
                File("_nordcommon_atlas_generator.bat"),
                File("_redoran_atlas_generator.bat"),
                File("_redware_atlas_generator.bat"),
                File("_redware_pot_atlas_generator.bat"),
                File("_shack_atlas_generator.bat"),
                File("_ship_atlas_generator.bat"),
                File("_silt_strider_atlas_generator.bat"),
                File("_silverware_atlas_generator.bat"),
                File("_sixth_house_atlas_generator.bat"),
                File("_skeleton_atlas_generator.bat"),
                File("_stat_saints_atlas_generator.bat"),
                File("_urn_atlas_generator.bat"),
                File("_velothi_atlas_generator.bat"),
                File("_w_mace_molag_bal_atlas_generator.bat"),
                File("_wood_docks_generator.bat"),
                File("_woodpoles_atlas_generator.bat"),
            ],
            S=MAIN_FILE,
            REQUIRED_USE="atlasgen",
        ),
        InstallDir("00 Core", S=MAIN_FILE),
        InstallDir("01 Textures - Vanilla", S="Project_Atlas-45399-0-7-1-1644694673"),
        InstallDir("02 Urns - Smoothed", S="Project_Atlas-45399-0-7-1-1644694673"),
        InstallDir("03 Redware - Smoothed", S="Project_Atlas-45399-0-7-1-1644694673"),
        InstallDir(
            "04 Emperor Parasols - Smoothed", S="Project_Atlas-45399-0-7-1-1644694673"
        ),
        InstallDir(
            "05 Wood Poles - Hi-Res Texture",
            RENAME="textures",
            S="Project_Atlas-45399-0-7-1-1644694673",
            REQUIRED_USE="texture_size_1024",
        ),
        InstallDir(
            "05 Wood Poles - Hi-Res Texture/ATL",
            RENAME="textures",
            S="Project_Atlas-45399-0-7-1-1644694673",
            REQUIRED_USE="texture_size_1024 !atlasgen",
        ),
        InstallDir("06 Glow in the Dahrk Patch", S=MAIN_FILE, REQUIRED_USE="gitd"),
        InstallDir(
            "07 Graphic Herbalism Patch",
            S="Project_Atlas-45399-0-7-1-1644694673",
            REQUIRED_USE="graphic-herbalism",
        ),
        InstallDir(
            "01 Textures - Intelligent Textures",
            S="Textures_-_Intelligent_Textures-45399-0-7-1-1644694801",
            REQUIRED_USE="intelligent-textures !atlasgen",
        ),
        InstallDir(
            "01 Textures - MET",
            S="Textures_-_MET-45399-0-7-1-1644695018",
            REQUIRED_USE="met !atlasgen",
        ),
        InstallDir(".", S="Hotfix_0.7.2-45399-0-7-2-1649834953"),
    ]

    def src_prepare(self):
        if sys.platform == "win32":
            print(
                "Trying to set run_without_pause to avoid pausing during atlas generation."
            )
            print("Not sure if this works as an environment variable")
            os.environ["run_without_pause"] = "1"
        super().src_prepare()
