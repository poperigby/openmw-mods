# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from pybuild import Pybuild2, patch_dir
from pybuild.info import PN, PV

VERSION_COMPONENTS = PV.split(".")
MY_PV = ".".join(VERSION_COMPONENTS[:-1]) + "-" + VERSION_COMPONENTS[-1]


class Package(Pybuild2):
    NAME = "ImageMagick"
    DESC = "A command line tool for manipulating images"
    HOMEPAGE = "https://imagemagick.org/"
    SRC_URI = f"""
        https://github.com/ImageMagick/ImageMagick/releases/download/{MY_PV}/ImageMagick--gcc-x86_64.AppImage -> {PN}-{MY_PV}-gcc-x86_64.AppImage
    """
    LICENSE = "imagemagick"
    KEYWORDS = "openmw"
    IUSE = "platform_linux"
    REQUIRED_USE = "platform_linux"
    DEPEND = "bin/7z"

    def src_unpack(self):
        for file in self.A:
            self.execute(["7z", "x", "-y", "-osquashfs-root", file.path])

    def src_install(self):
        patch_dir("squashfs-root/usr/bin", os.path.join(self.D, "bin"))
        patch_dir("squashfs-root/usr/lib", os.path.join(self.D, "lib"))
        patch_dir("squashfs-root/usr/share/doc", os.path.join(self.D, "doc"))
