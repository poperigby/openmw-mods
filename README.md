# The OpenMW Mod Repository

![pipeline](https://gitlab.com/portmod/openmw-mods/badges/master/pipeline.svg)
[![Build status](https://ci.appveyor.com/api/projects/status/d4ojy25ybwo2t98v/branch/master?svg=true&passingText=Windows%20OK&failingText=windows%20failed)](https://ci.appveyor.com/project/portmod/openmw-mods/branch/master)

The OpenMW Mod Repository stores pybuilds, a variation on [Gentoo's Ebuilds](https://wiki.gentoo.org/wiki/Ebuild).
These build files can be used by Portmod to install OpenMW mods.

See [Portmod](https://gitlab.com/portmod/portmod/) for details on how to use this repository to install mod packages.

An overview of the format can be found on the [Portmod Wiki](https://gitlab.com/portmod/portmod/wikis/).

If you are interested in contributing packages, see [CONTRIBUTING](https://gitlab.com/portmod/openmw-mods/-/blob/master/CONTRIBUTING.md)

Issues and bugs should be reported on the [GitLab Issue Tracker](https://gitlab.com/portmod/openmw-mods/-/issues) or via the [Email service desk](mailto:contact-project+portmod-openmw-mods-10338601-issue-@incoming.gitlab.com).
