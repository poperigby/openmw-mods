# Copyright 2019 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os

from common.mw import MW, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Monochrome User Interface"
    DESC = "Minimal flat white-on-black user interface overhaul."
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/45174"
    # Note that some assets are derived from Bethesda assets
    LICENSE = "free-use all-rights-reserved"
    RESTRICT = "mirror"  # At the very least uses a background from Bethesda's website.
    RDEPEND = "base/morrowind"
    KEYWORDS = "openmw"
    SRC_URI = "monochrome-user-interface-45174-1-3.7z"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/45174"
    TEXTURE_SIZES = "1773"
    INSTALL_DIRS = [InstallDir("monochrome-user-interface")]
    IUSE = "less-mono screen_aspect_4x3 screen_aspect_16x9 screen_aspect_16x10"

    def src_prepare(self):
        os.chdir("monochrome-user-interface")
        if "screen_aspect_4x3" in self.USE:
            os.replace(
                "Optional/Splash/Splash_Morrowind_4x3.tga",
                "Splash/Splash_Morrowind.tga",
            )
            os.replace(
                "Optional/Textures/menu_morrowind_4x3.dds", "Splash/menu_morrowind.dds"
            )
        elif "screen_aspect_16x9" in self.USE:
            os.replace(
                "Optional/Splash/Splash_Morrowind_16x9.tga",
                "Splash/Splash_Morrowind.tga",
            )
            os.replace(
                "Optional/Textures/menu_morrowind_16x9.dds", "Splash/menu_morrowind.dds"
            )
        elif "screen_aspect_16x10" in self.USE:
            os.replace(
                "Optional/Splash/Splash_Morrowind_16x10.tga",
                "Splash/Splash_Morrowind.tga",
            )
            os.replace(
                "Optional/Textures/menu_morrowind_16x10.dds",
                "Splash/menu_morrowind.dds",
            )

        # Extract ini changes from readme.txt
        section = "FontColor"
        self.FALLBACK = {section: {}}
        begin = False
        with open("README.md", "r") as config:
            for line in config.readlines():
                if (
                    not begin
                    and "Fully Monochrome" not in line
                    and "Less Monochrome" not in line
                ):
                    continue
                elif "Fully Monochrome" in line and "less-mono" not in self.USE:
                    begin = True
                elif "Less Monochrome" in line and "less-mono" in self.USE:
                    begin = True
                elif "Less Monochrome" in line and "less-mono" not in self.USE:
                    begin = False
                elif "List of included files" in line:
                    begin = False

                if begin:
                    if "=" in line and not line.startswith(";"):
                        key = line.split("=", 1)[0].strip()
                        value = line.split("=", 1)[1].strip()
                        self.FALLBACK[section][key] = value
