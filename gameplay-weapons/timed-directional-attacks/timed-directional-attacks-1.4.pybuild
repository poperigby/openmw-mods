# Copyright 2023 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Timed Directional Attacks"
    DESC = "Time your attacks well to get a buff depending on which directional attack you use"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/52195"
    LICENSE = "all-rights-reserved"
    RDEPEND = "base/morrowind"
    NEXUS_SRC_URI = """
        https://www.nexusmods.com/morrowind/mods/52195?tab=files&file_id=1000036224
        -> Solthas_Timed_Directional_Attacks-52195-1-4-1677728513.7z
    """
    KEYWORDS = "-openmw{<0.48}"
    INSTALL_DIRS = [
        InstallDir(
            "SolTimedDirAttacks",
            PLUGINS=[File("SolTimedDirAttacks.omwscripts")],
            DOC=["ReadMe STDA 1.4.md"],
        )
    ]

    def pkg_pretend(self):
        super().pkg_pretend()

        self.warn(
            "This mod only works with OpenMW 0.48 (currently a development build)"
        )
