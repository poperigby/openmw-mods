# Copyright 2019-2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3
from common.mw import MW, File, InstallDir
from common.nexus import NexusMod


class Package(NexusMod, MW):
    NAME = "Remiros' Groundcover"
    DESC = "Adds groundcover to almost all regions"
    HOMEPAGE = "https://www.nexusmods.com/morrowind/mods/46733"
    KEYWORDS = "openmw"
    LICENSE = "attribution-nc"
    NEXUS_URL = "https://www.nexusmods.com/morrowind/mods/46733"
    RDEPEND = """
        modules/openmw-config[grass]
        base/morrowind[bloodmoon,tribunal]
        tr? ( landmasses/tamriel-rebuilt[preview?] )
    """
    TEXTURE_SIZES = "512 1024"
    SRC_URI = "Remiros'_Groundcover_-_OpenMW-46733-2-0-1577400885.7z"
    IUSE = "tr preview +solstheim minimal"
    # With minimal enabled, the only things provided are the assets,
    # which are needed for base/tomb-of-the-snow-prince[grass]
    INSTALL_DIRS = [
        InstallDir(
            "00 Core",
            GROUNDCOVER=[
                File("Rem_BC.esp", REQUIRED_USE="!minimal"),
                File("Rem_WG.esp", REQUIRED_USE="!minimal"),
                File("Rem_GL.esp", REQUIRED_USE="!minimal"),
                File("Rem_Solstheim.esp", REQUIRED_USE="solstheim !minimal"),
                File("Rem_AI.esp", REQUIRED_USE="!minimal"),
                File("Rem_AC.esp", REQUIRED_USE="!minimal"),
                File("Rem_AL.esp", REQUIRED_USE="!minimal"),
            ],
        ),
        InstallDir(
            "01 TR Plugins",
            GROUNDCOVER=[
                File("Rem_TR_GL.esp"),
                File("Rem_TR_AI.esp"),
                File("Rem_TR_AT.esp"),
                File("Rem_TR_AC.esp"),
                File("Rem_TR_WG.esp"),
                File("Rem_TR_BC.esp"),
            ],
            REQUIRED_USE="tr !minimal",
        ),
        InstallDir(
            "02 TR Preview Plugins",
            GROUNDCOVER=[
                File("Rem_TRp_BC.esp"),
                File("Rem_TRp_AT.esp"),
                File("Rem_TRp_GM.esp"),
                File("Rem_TRp_TV.esp"),
                File("Rem_TRp_AI.esp"),
                File("Rem_TRp_GL.esp"),
                File("Rem_TRp_AL.esp"),
                File("Rem_TRp_RR.esp"),
                File("Rem_TRp_WG.esp"),
                File("Rem_TRp_Sol.esp"),
            ],
            REQUIRED_USE="preview !minimal",
        ),
        InstallDir(
            "03 Vanilla Resolution Textures",
            REQUIRED_USE="texture_size_512",
        ),
        InstallDir("04 Normal Maps"),
        # InstallDir(
        #     "05a Legend of Chemua",
        #     PLUGINS=[File("Rem_LoC.esp")],
        # ),
        # InstallDir(
        #     "05b Legend of Chemua Moved",
        #     PLUGINS=[File("Rem_LoCM.esp")],
        # ),
    ]
